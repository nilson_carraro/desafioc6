//
//  Constants.swift
//  DesafioC6NilsonCarraro
//
//  Created by Nilson Carraro on 15/04/19.
//  Copyright © 2019 NRC. All rights reserved.
//

import Foundation

class Constants {
    static let GITHUB_URL = "https://api.github.com/search/repositories?q=language:swift&sort=stars&page=%@&per_page=30"
}
