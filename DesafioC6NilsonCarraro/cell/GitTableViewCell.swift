//
//  GitTableViewCell.swift
//  DesafioC6NilsonCarraro
//
//  Created by Nilson Carraro on 15/04/19.
//  Copyright © 2019 NRC. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import Cosmos

class GitTableViewCell: UITableViewCell {

    @IBOutlet weak var fotoAutor: UIImageView!
    @IBOutlet weak var txtNomeRepo: UILabel!
    @IBOutlet weak var txtAutor: UILabel!
    @IBOutlet weak var cosmoStars: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setImage(link: String) {
        let url = URL(string: link)
        fotoAutor.kf.indicatorType = .activity
        let processor = DownsamplingImageProcessor(size: fotoAutor.frame.size)
            >> RoundCornerImageProcessor(cornerRadius: 20)
        
        fotoAutor.kf.setImage(
            with: url,
            placeholder: UIImage(named: "github-logo"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
    
    func setup(json: JSON) {
        let fotoURL = json["owner"]["avatar_url"].rawString() ?? ""
        setImage(link: fotoURL)
        
        txtNomeRepo.text = json["name"].stringValue
        txtAutor.text = json["owner"]["login"].stringValue
        cosmoStars.rating = json["score"].doubleValue > 5 ? 5 : json["score"].doubleValue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
