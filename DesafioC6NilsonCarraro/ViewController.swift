//
//  ViewController.swift
//  DesafioC6NilsonCarraro
//
//  Created by Nilson Carraro on 15/04/19.
//  Copyright © 2019 NRC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var listaItens: JSON!
    var currentPage = 1
    let reuseCell = "reuseCellID"
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "GitTableViewCell", bundle: nil), forCellReuseIdentifier: reuseCell)
        self.tableView.addSubview(self.refreshControl)
        self.getItens(page: 1)
    }
    
    func updateData() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getItens(page: 1)
    }

    func getItens(page: Int) {
        currentPage = page
        print(String(format: Constants.GITHUB_URL, "\(page)"))
        Alamofire.request(String(format: Constants.GITHUB_URL, "\(page)")).responseJSON { response in
            if let result = response.result.value {
                let swiftyJsonVar = JSON(result)
                let listagem = swiftyJsonVar["items"]
                if page == 1 {
                    self.listaItens = listagem
                } else {
                    self.listaItens.arrayObject!.append(contentsOf: listagem.arrayObject ?? [])
                }
                
                self.updateData()
            } else {
                self.updateData()
            }
        }
    }
    
    func getItensCount() -> Int {
        return listaItens == nil ? 0 : listaItens.arrayObject?.count ?? 0
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getItensCount()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCell) as! GitTableViewCell
        cell.setup(json: listaItens[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.getItensCount() - 5 {
            self.getItens(page: self.currentPage + 1)
        }
    }
}
